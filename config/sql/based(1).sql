-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 08, 2017 at 05:50 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `based`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

DROP TABLE IF EXISTS `tbl_message`;
CREATE TABLE IF NOT EXISTS `tbl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderID` varchar(50) NOT NULL,
  `receiverID` varchar(50) NOT NULL,
  `messageText` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`id`, `senderID`, `receiverID`, `messageText`) VALUES
(1, '', '', ''),
(2, '', '', ''),
(3, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message three'),
(4, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message four'),
(5, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message five'),
(6, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message six'),
(7, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message seven'),
(8, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message seven'),
(9, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message eight'),
(10, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message nine'),
(11, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message nine'),
(12, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message 10'),
(13, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message 10'),
(14, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message 10'),
(15, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this is message 11'),
(16, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this'),
(17, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this'),
(18, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this was'),
(19, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this was'),
(20, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this was'),
(21, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this now'),
(22, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this now ok'),
(23, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this now ok is it'),
(24, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'this now ok is it 2'),
(25, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'alright'),
(26, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'good'),
(27, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'nicedadjkfhdj'),
(28, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'shahed mama'),
(29, 'rayhan1508@cseku.ac.bd', 'test@test.com', 'are you?'),
(30, 'rayhan1508@cseku.ac.bd', 'test@test.com', 'i am'),
(31, 'rayhan1508@cseku.ac.bd', 'test@test.com', 'good'),
(32, 'test@test.com', 'rayhan1508@cseku.ac.bd', 'well'),
(33, 'rayhan1508@cseku.ac.bd', 'test@test.com', 'how are you'),
(34, 'rayhan1508@cseku.ac.bd', 'test@test.com', 'pretty well'),
(35, 'test@test.com', 'rayhan1508@cseku.ac.bd', 'hello'),
(36, 'rayhan1508@cseku.ac.bd', 'another@test.com', 'no conversation with me '),
(37, 'another@test.com', 'rayhan1508@cseku.ac.bd', 'now i am replying');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message_group`
--

DROP TABLE IF EXISTS `tbl_message_group`;
CREATE TABLE IF NOT EXISTS `tbl_message_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `group_member_id` varchar(50) NOT NULL,
  `group_member_role` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message_group`
--

INSERT INTO `tbl_message_group` (`group_id`, `group_name`, `group_member_id`, `group_member_role`) VALUES
(2, 'Group Name', 'another@test.com', 'standard'),
(11, 'megneticrobot', 'rayhan1508@cseku.ac.bd', 'admin'),
(10, 'Robot', 'shahed1509@cseku.ac.bd', 'standard'),
(5, 'Group Name 2', 'another@test.com', 'standard'),
(6, 'Group Name 2', 'test@test.com', 'standard'),
(7, 'Group Name 2', 'shahed1509@cseku.ac.bd', 'standard'),
(12, 'megneticrobot', 'another@test.com', 'standard'),
(13, 'megneticrobot', 'shahed1509@cseku.ac.bd', 'standard'),
(14, 'megneticrobot', 'test@test.com', 'standard'),
(15, 'new group', 'rayhan1508@cseku.ac.bd', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message_group_message`
--

DROP TABLE IF EXISTS `tbl_message_group_message`;
CREATE TABLE IF NOT EXISTS `tbl_message_group_message` (
  `group_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `senderID` varchar(50) NOT NULL,
  `messageText` varchar(50) NOT NULL,
  PRIMARY KEY (`group_message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message_group_message`
--

INSERT INTO `tbl_message_group_message` (`group_message_id`, `group_name`, `senderID`, `messageText`) VALUES
(1, 'Group Name', 'rayhan1508@cseku.ac.bd', 'hello'),
(2, 'Group Name', 'rayhan1508@cseku.ac.bd', 'hello 2'),
(3, 'Group Name', 'rayhan1508@cseku.ac.bd', 'nice'),
(4, 'Group Name', 'rayhan1508@cseku.ac.bd', 'great'),
(5, 'Group Name', 'test@test.com', 'wao'),
(6, 'Group Name', 'rayhan1508@cseku.ac.bd', 'he he he'),
(7, 'Group Name', 'test@test.com', 'ho ho ho'),
(8, 'Group Name 2', 'rayhan1508@cseku.ac.bd', 'this is new group'),
(9, 'Group Name 2', 'test@test.com', 'i am member og thiis group'),
(10, 'Group Name 2', 'another@test.com', 'i am joined'),
(11, 'Group Name 2', 'rayhan1508@cseku.ac.bd', 'happy to see you both'),
(12, 'Group Name 2', 'rayhan1508@cseku.ac.bd', 'this is navin reddy'),
(13, 'Group Name 2', 'another@test.com', 'welcome back aliens'),
(14, 'megneticrobot', 'rayhan1508@cseku.ac.bd', 'hello'),
(15, 'megneticrobot', 'shahed1509@cseku.ac.bd', 'hi');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
