<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/com/modules/message/dao/class.messagedao.php'; ?>

<?php

class MessageBao {

	private $messageDao;

	public function __construct() {
		$this->messageDao = new MessageDao();
	}

	public function getAllUser() {
		return $this->messageDao->getAllUser();
	}

	public function getSearchedUser($search_query) {
		return $this->messageDao->getSearchedUser($search_query);
	}

	public function getReceiverDeatails($receiver) {
		return $this->messageDao->getReceiverDeatails($receiver);	
	}

	public function createGroup($messageGroup) {
		return $this->messageDao->createGroup($messageGroup);
	}

	public function getAllGroup($groupMemberID) {
		return $this->messageDao->getAllGroup($groupMemberID);
	}
}

?>