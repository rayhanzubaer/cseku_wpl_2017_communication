<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/com/common/class.common.message.php'; ?>

<?php

class MessageDao {
	private $db;
	private $message;
	private $messageGroup;
	private $messageGroupMessage;

	public function __construct() {
		$this->db = DB::getInstance();
		$this->message = new Message();
		$this->messageGroup = new MessageGroup();
		$this->messageGroupMessage = new MessageGroupMessage();
	}

	public function getAllUser() {
		$sql = "SELECT * FROM tbl_user";
		$result = $this->db->select($sql);
		
		return $result;
	}

	public function getSearchedUser($search_query) {
		$sql = "SELECT * FROM tbl_user WHERE Email like '%$search_query%'";
		$result = $this->db->select($sql);
		
		return $result;
	}

	public function getReceiverDeatails($receiver) {
		$sql = "SELECT * FROM tbl_user WHERE ID='$receiver'";
		$result = $this->db->selectFirst($sql);

		return $result;	
	}

	public function createGroup($messageGroup) {
		$this->messageGroup = $messageGroup;

		$groupName = $this->messageGroup->getGroupName();
		$groupMember = $this->messageGroup->getGroupMember();
		$groupMemberRole = $this->messageGroup->getGroupMemberRole();

		$sql = "INSERT INTO tbl_message_group(group_name, group_member_id, group_member_role) VALUES ";
		
		for ($i=0; $i < count($groupMember); $i++) {
			$sql .= "('$groupName', '$groupMember[$i]', '$groupMemberRole[$i]')";

			if ($i + 1 < count($groupMember)) {
				$sql .=', ';
			}
		}
		return $this->db->insert($sql);
	}

	public function getAllGroup($groupMemberID) {
		$sql = "SELECT * FROM tbl_message_group WHERE group_member_id='$groupMemberID'";
		$result = $this->db->select($sql);

		return $result;
	}
}

?>