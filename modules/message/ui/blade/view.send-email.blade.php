<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/com/modules/message/bao/class.messagebao.php';
?>

<?php

$message = new Message();
$messageGroup = new MessageGroup();
$messageGroupMessage = new MessageGroupMessage();
$messageBao = new MessageBao();

if (isset($_POST['submit'])) {
	$file = $_FILES['file']['tmp_name'];
	$receiver = $_POST['h-receiver'];
	$subject = $_POST['subject'];
	$body = $_POST['body'];
	$receiverName = '';

	for ($i = 0; $i < count($receiver); $i++) {
		$receiverName .= $receiver[$i];

		if ($i + 1 < count($receiver)) {
			$receiverName .=', ';
		}
	}

	if (mail($receiverName, $subject, $body, $file)) {
		echo '<p class="alert alert-success">Successful.</p>';
	}
	else {
		echo '<p class="alert alert-danger">Failed.</p>';	
	}
}

// get group list
$groupMemberID = $_SESSION['globalUser']->getID();
$groupList = $messageBao->getAllGroup($groupMemberID);

?>